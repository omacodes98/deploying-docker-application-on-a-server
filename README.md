# Deploy Application on Server With Docker

- Deploy Docker application on a server with Docker Compose 

## Table of Contents
- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Copied Docker-compose file to remote server 

* Logged into private Docker registry on remote server to fetch app image

* Started application container with MongoDB and MongoExpress services using docker compose 

## Technologies Used 

* Docker 

* Amazon ECR

* Node.js 

* MongoDB 

* Mongo Express 

## Steps 

Step 1: Log into AWS 

     docker aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 522116691231.dkr.ecr.eu-west-2.amazonaws.com

[Log into AWS](/images/01_logged_into_aws_docker_repo.png)

Step 2: Copying yaml file to server 

     cat docker-compose.yaml > mongo.yaml 
     or 
     scp -P 22 mongo.yaml student@192.168.0.20:~

[Copying Yaml file](/images/02_copying_for_server.png)

Step 3: Run containers in Server 

    docker-compose -f mongo.yaml up

[Container running in server](/images/03_running_containers_server.png)
[Mongodb Active UI](/images/04_mongodb_active_UI.png)


## Installation

* curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"

* sudo installer -pkg AWSCLIV2.pkg -target /

## Usage 

Run $ node server.js

localhost:8081

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/deploying-docker-application-on-a-server.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/omacodes98/deploying-docker-application-on-a-server

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.